<%-- 
    Document   : index
    Created on : Nov 15, 2013, 7:08:32 PM
    Author     : paulo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE HTML>
<!--
	Miniport 2.0 by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>PC - DREAMS, We make your dream software reality</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,600,700" rel="stylesheet" />
		<script src="js/jquery.min.js"></script>
		<script src="js/config.js"></script>
		<script src="js/skel.min.js"></script>
                <script src="js/main.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel-noscript.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-desktop.css" />
		</noscript>
		<!--[if lte IE 9]><link rel="stylesheet" href="css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><script src="js/html5shiv.js"></script><link rel="stylesheet" href="css/ie8.css" /><![endif]-->
		<!--[if lte IE 7]><link rel="stylesheet" href="css/ie7.css" /><![endif]-->
	</head>
	<body>

		<!-- Nav -->
			<nav id="nav">
				<ul class="container">
					<li><a href="#top">Home</a></li>
					<li><a href="#work">Apps</a></li>
					<li><a href="#portfolio">Portfolio</a></li>
					<li><a href="#contact">Contact</a></li>
                                        <li><a href="./pt"><img src="images/portugal.png" width="24" height="24" alt="Português" title="Português"/></a></li>
                                        
				</ul>
                            
			</nav>
                
		<!-- Home -->
			<div class="wrapper wrapper-style1 wrapper-first">
				<article class="container" id="top">
					<div class="row">
						<div class="4u">
                                                    <span class="me">
                                                        <img width="217px" height="159px" src="images/pcdreams.png" alt="" />
                                                    </span>
                                                    <!--<img style="position:absolute;left:20px;top:40px; z-index:200" src="images/stamp.png" alt="" />-->
						</div>
						<div class="8u">
							<header>
								<h1><strong>PC Dreams Software</strong></h1>
							</header>
							<p>Welcome to <strong>PC Dreams Software</strong>, home of beautiful apps.<br/>
                                                        The place were software dreams come to life!
                                                        </p>
							<a href="#work" class="button button-big">Learn about what we do</a>
						</div>
					</div>
				</article>
			</div>

		<!-- Work -->
			<div class="wrapper wrapper-style2">
				<article id="work">
					<header>
						<h2>We design and build amazing things.</h2>
						<span>From desktop, to mobile, to web. You dream it, we build it!<br/>
                                                    Our dreams, our apps. Your dreams, your apps. Check our portfolio to get an insight.</span>
					</header>
					<div class="container">
						<div class="row">
							<div class="4u">
								<section class="box box-style1">
									<!--<span class="icon featured-icon icon-comments-alt"></span>-->
                                                                        <img src="images/ic_launcher.png"/>
									<h3>EasyMessage</h3>
									<p class="align_justify">EasyMessage is a free messaging app for Android, iPhone and iPad devices.<br/>
                                                                        It lets you send messages through multiple services (SMS & Email) and post to social networks (Twitter, Facebook & LinkedIn) at the same time,
                                                                         writing just once. You can build your custom audience as groups and save commonly used messages in archive for later usage.
                                                                        </p>
								</section>
							</div>
							<div class="4u">
								<section class="box box-style1">
									<!--<span class="icon featured-icon icon-file-alt"></span>-->
                                                                        <img src="images/logo_big.png"/>
									<h3>AEMS</h3>
									<p class="align_justify">AEMS (short for Advanced Event Management System) is the world first 360º event management and registration suite. 
                                                                        It´s a full featured and generic suite that includes project management functionalities and offers event organizers the possibility to track every steps and resources involved in 
                                                                        event planning, execution and analysis. 
                                                                        </p>
								</section>
							</div>
                                                        <div class="4u">
								<section class="box box-style1">
                                                                    <img src="images/flapi.png" width="100" height="100"/>
									<h3>Flapi - The Bird Fish!</h3>
									<p class="align_justify">Flapi - The Bird Fish is a fun mobile game for iPhone and iPad devices. 
                                                                           Flapi is golden fish whose pound suddenly dry out of water, so he needed urgently to find a new home to live.
                                                                           He was able to escape with the help of a jet pack, but you need to fly him safely through all the obstacles that are in his way. 
                                                                           Be part of the Flapi journey, see how far you can go, play with friends. Have fun!
                                                                            <br/></p>
								</section>
							</div><br/>
                                                       <div class="4u"><br/>
								<section class="box box-style1">
                                                                    <img src="images/clones.png" width="100" height="100"/>
									<h3>Revenge Of The Clones!</h3>
									<p class="align_justify">Revenge Of The Clones is a fun mobile game for iPhone and iPad devices. 
                                                                            This entertaining arcade game is a parody to the Flappy bird game hit and all his clones, including <a target="_blank" href="https://itunes.apple.com/us/app/flapi-the-bird-fish/id837165900?ls=1&mt=8">Flapi - The Bird Fish</a>. 
                                                                           The birds are back and they want revenge from the pipes. They came with bombs and they are trying to destroy the city of the pipes. 
                                                                           Only you and the amazing flying pipe can save the city. Beat them away and make them scream while they're hit by their own bombs.
                                                                            <br/><br/></p>
								</section>
							</div>
                                                        <div class="4u"><br/>
								<section class="box box-style1">
                                                                    <img src="images/alien_player.png" width="100" height="100"/>
                                                                    <h3>Alfie - The Angry Alien Saga! </h3>
									<p class="align_justify">Alfie - The Angry Alien Saga is a space shooter alike mobile game for IOS (iPhone and iPad) and Android devices. 
                                                                            Alfie is an explorer and adventurous alien that went on a journey to explore a small planet, recently discovered. Unfortunately 
                                                                             the space ship engine was hit and damaged by a small asteroid, near the planet's orbit, and he got trapped in a very dangerous place...
                                                                             <!--But the worst thing was that 
                                                                             the asteroids belt was habitated by strange (and very unfriendly) slug green creatures. These creatures are trying to kill Alfie's and hijack his spacecraft.
                                                                             The only way for Alfie to escape alive is to collect all the energy stars and repair the engine.-->
                                                                            <br/><br/></p>
								</section>
							</div>
                                                    
                                                    <div class="4u"><br/>
								<section class="box box-style1">
                                                                    <img src="images/jelly.png" width="100" height="100"/>
                                                                    <h3>Jelly Crush Party! </h3>
									<p class="align_justify">Coming soon for IOS (iPhone and iPad) devices. 
                                                                            <img src="images/jellypromo.png" width="300" height="389"/>
                                                                            <br/><br/></p>
								</section>
							</div>
							
                                                    
						</div>
					</div>
					<footer>
                                            <h4>List of companies we worked with along the way (*)</h4>
                                            
                                            <ul class="ul_left_align">
                                            <li>Glintt SA (Portugal)</li>
                                            <li>SAPO, PT Comunicações SA (Portugal)</li>
                                            <li>Opensource Strategies Inc. (United States)</li>
                                            <li>Simchronise Limited (Ireland)</li>
                                            <li>Tapastreet Limited (Ireland)</li>
                                            <li>Funambol Inc. (United States)</li>
                                            <li>Others...</li>
                                            </ul>
                                            <span style="margin:0;padding:0">(*) Projects involve mobile, web and desktop apps.</span>
                                            <br style="line-height:4px;"/>
					
						<a href="#portfolio" class="button button-big">See some of our recent work</a>
					</footer>
				</article>
			</div>

		<!-- Portfolio -->
			<div class="wrapper wrapper-style3">
                               <article id="portfolio">
					<header>
                                            <h2>
                                                <img style="vertical-align:middle; margin-left:5px" width="100" height="100" src="images/flapi.png"/>
                                                Flapi - The Bird Fish <sup>&trade;</sup><a href="https://itunes.apple.com/us/app/flapi-the-bird-fish/id837165900?ls=1&mt=8"><img style="vertical-align:middle; margin-left:5px" src="images/available_app_store_200px.png"/></a>
                                             </h2>
                                                
						<span>Flapi - The Fish Bird is a fun mobile game for IOS Platforms (iPhone/iPad)<br/>
                                                    Flapi is fish with a jet pack. With a simple "touch" and "go" mechanic you guide him on his ultimate quest for a new pound.
                                                </span>
                                                
					</header>
					<div class="container">
						<div class="row">
							<div class="12u">
							</div>
						</div>
						<div class="row">
							<div class="4u">
								<article class="box box-style2">
									<a href="https://itunes.apple.com/us/app/flapi-the-bird-fish/id837165900?ls=1&mt=8" class="image image-full"><img src="images/flapi5.png" alt="" /></a>
									<h3><a href="https://itunes.apple.com/us/app/flapi-the-bird-fish/id837165900?ls=1&mt=8">Tap & Fly</a></h3>
									<p>With a very simple but fun mechanic you can take Flapi to his new home.</p>
								</article>
							</div>
							<div class="4u">
								<article class="box box-style2">
									<a href="https://itunes.apple.com/us/app/flapi-the-bird-fish/id837165900?ls=1&mt=8" class="image image-full"><img src="images/flapi2.png" alt="" /></a>
									<h3><a href="https://itunes.apple.com/us/app/flapi-the-bird-fish/id837165900?ls=1&mt=8">Avoid the Obstacles</a></h3>
									<p>You need to avoid all the obstacles on the way, just fly through the intervals.</p>
								</article>
							</div>
							<div class="4u">
								<article class="box box-style2">
									<a href="https://itunes.apple.com/us/app/flapi-the-bird-fish/id837165900?ls=1&mt=8" class="image image-full"><img src="images/flapi4.png" alt="" /></a>
									<h3><a href="https://itunes.apple.com/us/app/flapi-the-bird-fish/id837165900?ls=1&mt=8">Go for Achievements</a></h3>
									<p>Se how far you can go. Challenge your friends to do better than you.</p>
								</article>
							</div>
						</div>
						<!--<div class="row">
							
						</div>-->
					</div>
					<footer>
						<!--<p></p>
						<a href="#contact" class="button button-big">Get in touch with us</a>-->
					</footer>
				</article>
                            
                               <!-- revenge of clones -->
                               <article id="portfolio">
					<header>
                                            <h2>
                                                <img style="vertical-align:middle; margin-left:5px" width="100" height="100" src="images/clones.png"/>
                                                Revenge Of The Clones <sup>&trade;</sup><a href="https://itunes.apple.com/us/app/revenge-of-the-clones/id859897918?mt=8"><img style="vertical-align:middle; margin-left:5px" src="images/available_app_store_200px.png"/></a>
                                             </h2>
                                                
						<span>Revenge Of The Clones is an addictive game for IOS Platforms (iPhone/iPad)<br/>
                                                    This is a parody to the Flappy bird game and all the related clones. <br/>The birds are back equiped with bombs and they want revenge from the pipes.
                                                </span>
                                                
					</header>
					<div class="container">
						<div class="row">
							<div class="12u">
							</div>
						</div>
						<div class="row">
							<div class="4u">
								<article class="box box-style2">
									<a href="https://itunes.apple.com/us/app/revenge-of-the-clones/id859897918?mt=8" class="image image-full"><img src="images/clones1.png" alt="" /></a>
									<h3><a href="https://itunes.apple.com/us/app/revenge-of-the-clones/id859897918?mt=8">Keep the bombs away</a></h3>
									<p>Protect the city of pipes. Avoid the crazy bird bombers.</p>
								</article>
							</div>
							<div class="4u">
								<article class="box box-style2">
									<a href="https://itunes.apple.com/us/app/revenge-of-the-clones/id859897918?mt=8" class="image image-full"><img src="images/clones2.png" alt="" /></a>
									<h3><a href="https://itunes.apple.com/us/app/revenge-of-the-clones/id859897918?mt=8">Hit them with the pipe</a></h3>
									<p>Swing the flying pipe. Beat them as they approach the city.</p>
								</article>
							</div>
							<div class="4u">
								<article class="box box-style2">
									<a href="https://itunes.apple.com/us/app/revenge-of-the-clones/id859897918?mt=8" class="image image-full"><img src="images/clones3.png" alt="" /></a>
									<h3><a href="https://itunes.apple.com/us/app/revenge-of-the-clones/id859897918?mt=8">Set them on fire</a></h3>
									<p>Make them taste their own medicine. Blown them away and make them run away screaming.</p>
								</article>
							</div>
						</div>
						<!--<div class="row">
							
						</div>-->
					</div>
					<footer>
						<!--<p></p>
						<a href="#contact" class="button button-big">Get in touch with us</a>-->
					</footer>
				</article>
                               
                               
                               <!-- Alfie - The Angry Alien Saga -->
                               <article id="portfolio">
					<header>
                                            <h2>
                                                <img style="vertical-align:middle; margin-left:5px" width="100" height="100" src="images/alien_player.png"/>
                                                Alfie - The Angry Alien Saga <sup>&trade;</sup><a href="https://itunes.apple.com/us/app/alfie-the-angry-alien-saga/id827406444?mt=8"><img style="vertical-align:middle; margin-left:5px" src="images/available_app_store_200px.png"/></a>
                                               <a href="https://play.google.com/store/apps/details?id=com.pcristo.flappyalien"><img style="vertical-align:middle; margin-left:5px" src="images/en_generic_rgb_wo_60.png"/></a> 
                                            </h2>
                                                
						<span>Alfie - The Angry Alien Saga is a space shooter game for IOS Platforms (iPhone/iPad) and Android.<br/>
                                                    On one of his intrepidous quests he ended up trapped on a dangerous place. And now he just wants to return home.
                                                </span>
                                                
					</header>
					<div class="container">
						<div class="row">
							<div class="12u">
							</div>
						</div>
						<div class="row">
							<div class="4u">
								<article class="box box-style2">
									<a href="https://itunes.apple.com/us/app/alfie-the-angry-alien-saga/id827406444?mt=8" class="image image-full"><img src="images/Alfie1.png" alt="" /></a>
									<h3><a href="https://itunes.apple.com/us/app/alfie-the-angry-alien-saga/id827406444?mt=8">Bring Alfie home</a></h3>
									<p>Help Alfie to return safe and sound to his family. May the gaming force be with you!</p>
								</article>
							</div>
							<div class="4u">
								<article class="box box-style2">
									<a href="https://itunes.apple.com/us/app/alfie-the-angry-alien-saga/id827406444?mt=8" class="image image-full"><img src="images/Alfie2.png" alt="" /></a>
									<h3><a href="https://itunes.apple.com/us/app/alfie-the-angry-alien-saga/id827406444?mt=8">Collect all energy stars</a></h3>
									<p>Collect all the energy stars needed to repair the spacecraft engine.</p>
								</article>
							</div>
							<div class="4u">
								<article class="box box-style2">
									<a href="https://itunes.apple.com/us/app/alfie-the-angry-alien-saga/id827406444?mt=8" class="image image-full"><img src="images/Alfie3.png" alt="" /></a>
									<h3><a href="https://itunes.apple.com/us/app/alfie-the-angry-alien-saga/id827406444?mt=8">Avoid the enemies</a></h3>
									<p>Shoot the evil the alien slugs. Watch out for the asteroids field.</p>
								</article>
							</div>
						</div>
						<!--<div class="row">
							
						</div>-->
					</div>
					<footer>
						<!--<p></p>
						<a href="#contact" class="button button-big">Get in touch with us</a>-->
					</footer>
				</article>
                            
                            
				<article id="portfolio">
					<header>
                                            <h2>EasyMessage <sup>&trade;</sup><a href="https://itunes.apple.com/us/app/easymessage/id668776671?ls=1&mt=8"><img style="vertical-align:middle; margin-left:5px" src="images/available_app_store_200px.png"/></a>
                                                <a href="https://play.google.com/store/apps/details?id=com.pcristo.easymessage"><img style="vertical-align:middle; margin-left:5px" src="images/en_generic_rgb_wo_60.png"/></a>
                                             </h2>
                                                
						<span>EasyMessage is an utilities app that makes messaging simpler and funnier than ever before!<br/>
                                                    Write just one message and the app will know exactly how each recipient should be reached.<br/>
                                                    Email, SMS and social networks together in just one app!
                                                </span>
                                                
					</header>
					<div class="container">
						<div class="row">
							<div class="12u">
							</div>
						</div>
						<div class="row">
							<div class="4u">
								<article class="box box-style2">
									<a href="https://itunes.apple.com/us/app/easymessage/id668776671?ls=1&mt=8" class="image image-full"><img src="images/archive.png" alt="" /></a>
									<h3><a href="https://itunes.apple.com/us/app/easymessage/id668776671?ls=1&mt=8">Send and Archive</a></h3>
									<p>Use pre-defined messages to save time. Save your own custom messages.</p>
								</article>
							</div>
							<div class="4u">
								<article class="box box-style2">
									<a href="https://itunes.apple.com/us/app/easymessage/id668776671?ls=1&mt=8" class="image image-full"><img src="images/easy1.png" alt="" /></a>
									<h3><a href="https://itunes.apple.com/us/app/easymessage/id668776671?ls=1&mt=8">Write once and Send</a></h3>
									<p>Reach all your recipients at once, either via Email, SMS, Facebook, Twitter or LinkedIn.</p>
								</article>
							</div>
							<div class="4u">
								<article class="box box-style2">
									<a href="https://itunes.apple.com/us/app/easymessage/id668776671?ls=1&mt=8" class="image image-full"><img src="images/easy2.png" alt="" /></a>
									<h3><a href="https://itunes.apple.com/us/app/easymessage/id668776671?ls=1&mt=8">Manage contacts lists</a></h3>
									<p>Manage your contacts in a easy way. Create meaningful groups.</p>
								</article>
							</div>
						</div>
						<!--<div class="row">
							
						</div>-->
					</div>
					<footer>
						<!--<p></p>
						<a href="#contact" class="button button-big">Get in touch with us</a>-->
					</footer>
				</article>
                                <!-- AEMS -->
                                <article id="portofolio">
                                <header>
                                            <h2><img style="vertical-align:middle; margin-left:5px" src="images/logo_big.png"/>AEMS - Advanced Event Management System</h2>
                                                
						<span>360º Event Management and Registration with Project Management Features!<br/>
                                                    AEMS is the most complete, affordable and generic event management suite in the market.<br/>
                                                    <a href="http://www.advancedeventmanagement.com">www.advancedeventmanagement.com</a>
                                                </span>
                                                
					</header>
					<div class="container">
						<div class="row">
							<div class="12u">
							</div>
						</div>
						<div class="row">
							<div class="4u">
								<article class="box box-style2">
									<a href="http://www.advancedeventmanagement.com" class="image image-full"><img src="images/home_screen.png" alt="" /></a>
									<h3><a href="http://www.advancedeventmanagement.com">Zer0&trade; <br/>Configuration System</a></h3>
									<p>AEMS Works "Out-of-the-box". No installation or configuration required, just unzip and start! AEMS can run both online or offline.</p>
                                                                </article>
							</div>
							<div class="4u">
								<article class="box box-style2">
									<a href="http://www.advancedeventmanagement.com" class="image image-full"><img src="images/main_login_screen.png" alt="" /></a>
									<h3><a href="http://www.advancedeventmanagement.com">Generic, Affordable and Customizable</a></h3>
									<p>AEMS is "white label" ready and fully customizable. Generic and complete, it fits all event types or company sizes.
                                                                        
                                                                        </p>
								</article>
							</div>
							<div class="4u">
								<article class="box box-style2">
									<a href="http://www.advancedeventmanagement.com" class="image image-full"><img src="images/home_screen_imac.png" alt="" /></a>
									<h3><a href="http://www.advancedeventmanagement.com">Cross-platform</a></h3>
									<p>AEMS runs on Windows, Linux and Mac. Accessible anytime/anywhere, either on your pc, laptop or smartphone.</p>
								</article>
							</div>
						</div>
						
					</div>
					<footer>
						<p></p>
						<a href="#contact" class="button button-big">Get in touch with us</a>
					</footer>
				</article>
			</div>

		<!-- Contact -->
			<div class="wrapper wrapper-style4">
				<article id="contact" class="container small">
					<header>
						<h2>Want to hire us? Get in touch!</h2>
                                                <span>Use either the online form bellow or mail us directly: <a href="mailto:info@pcdreams-software.com">info[at]pcdreams-software.com </a>.</span>
					</header>
					<div>
						<div class="row">
							<div class="12u">
                                                            <form onsubmit="return validateForm();" method="post" name="formcontact" action="contact?action=contact">
									<div>
										<div class="row half">
											<div class="6u">
												<input type="text" name="name" id="name" placeholder="Name" />
											</div>
											<div class="6u">
												<input type="text" name="email" id="email" placeholder="Email" />
											</div>
										</div>
										<div class="row half">
											<div class="12u">
												<input type="text" name="subject" id="subject" placeholder="Subject" />
											</div>
										</div>
										<div class="row half">
											<div class="12u">
												<textarea name="message" id="message" placeholder="Message"></textarea>
											</div>
                                                                                    (*) All fields are mandatory
										</div>
										<div class="row">
                                                                                    
											<div class="12u">
												<a href="#" class="button form-button-submit">Send Message</a>
												<a href="#" class="button button-alt form-button-reset">Clear Form</a>
											</div>
										</div>
									</div>
                                                                    
								</form>
                                                            
							</div>
						</div>
						<div class="row row-special">
							<div class="12u">
								<h3>Find us on ...</h3>
								<ul class="social">
									<li class="twitter"><a href="https://twitter.com/pcdreamsapps" class="icon icon-twitter"><span>Twitter</span></a></li>
									<li class="facebook"><a href="https://www.facebook.com/pages/PC-Dreams-Software/557351897713700" class="icon icon-facebook"><span>Facebook</span></a></li>
									<!--<li class="dribbble"><a href="http://dribbble.com/n33" class="icon icon-dribbble"><span>Dribbble</span></a></li>-->
									<li class="linkedin"><a href="http://pt.linkedin.com/pub/paulo-cristo/4/b90/755/" class="icon icon-linkedin"><span>LinkedIn</span></a></li>
									<!--<li class="tumblr"><a href="#" class="icon icon-tumblr"><span>Tumblr</span></a></li>
									<li class="googleplus"><a href="#" class="icon icon-google-plus"><span>Google+</span></a></li>
									<li class="github"><a href="http://github.com/n33" class="icon icon-github"><span>Github</span></a></li>-->
									<!--
									<li class="rss"><a href="#" class="icon icon-rss"><span>RSS</span></a></li>
									<li class="instagram"><a href="#" class="icon icon-instagram"><span>Instagram</span></a></li>
									<li class="foursquare"><a href="#" class="icon icon-foursquare"><span>Foursquare</span></a></li>
									<li class="skype"><a href="#" class="icon icon-skype"><span>Skype</span></a></li>
									<li class="soundcloud"><a href="#" class="icon icon-soundcloud"><span>Soundcloud</span></a></li>
									<li class="youtube"><a href="#" class="icon icon-youtube"><span>YouTube</span></a></li>
									<li class="blogger"><a href="#" class="icon icon-blogger"><span>Blogger</span></a></li>
									<li class="flickr"><a href="#" class="icon icon-flickr"><span>Flickr</span></a></li>
									<li class="vimeo"><a href="#" class="icon icon-vimeo"><span>Vimeo</span></a></li>
									-->
								</ul>
							</div>
						</div>
					</div>
                                    <footer>
                                            <p id="copyright">
							&copy; 2014 PCDreams-Software  | Design based on : <a href="http://html5up.net/">HTML5 UP</a>
						</p>
				    </footer>
				</article>
			</div>


	</body>
</html>
