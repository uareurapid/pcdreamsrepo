<%-- 
    Document   : login
    Created on : Mar 18, 2011, 12:31:00 PM
    Author     : paulo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>PC - DREAMS, We make your dream software reality</title>  
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,600,700" rel="stylesheet" />
		<script src="js/jquery.min.js"></script>
		<script src="js/config.js"></script>
		<script src="js/skel.min.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel-noscript.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-desktop.css" />
		</noscript>
    <script type="text/JavaScript">

var redirectTime = "2000";
redirectURL = "<%=request.getContextPath()%>/index";
function redirect() {

	setTimeout("location.href = redirectURL;",redirectTime);
}

</script>
    </head>
    <body onload="redirect();">
     
      <p>&nbsp;</p>
      <h3>Dear <em><%=session.getAttribute("name")%></em> ...</h3>
      Thank you for contacting me! I promise to be back to you as soon as possible.<br/>
      You should now be back to the main page in a couple of seconds. If not please click <a style="font-weight: bold;" href="/">here!</a> <br/>
      <br/>

    </body>
</html>

