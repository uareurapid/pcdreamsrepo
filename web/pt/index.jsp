<%-- 
    Document   : index
    Created on : Nov 15, 2013, 7:08:32 PM
    Author     : paulo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE HTML>
<!--
	Miniport 2.0 by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>PC - DREAMS, Fazemos do seu sonho de software realidade</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,600,700" rel="stylesheet" />
		<script src="js/jquery.min.js"></script>
		<script src="js/config.js"></script>
		<script src="js/skel.min.js"></script>
                <script src="js/main.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel-noscript.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-desktop.css" />
		</noscript>
		<!--[if lte IE 9]><link rel="stylesheet" href="css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><script src="js/html5shiv.js"></script><link rel="stylesheet" href="css/ie8.css" /><![endif]-->
		<!--[if lte IE 7]><link rel="stylesheet" href="css/ie7.css" /><![endif]-->
	</head>
	<body>

		<!-- Nav -->
			<nav id="nav">
				<ul class="container">
					<li><a href="#top">Início</a></li>
					<li><a href="#work">Aplicações</a></li>
					<li><a href="#portfolio">Portfólio</a></li>
					<li><a href="#contact">Contacto</a></li>
                                        <li><a href="../"><img src="images/england.png" width="24" height="24" alt="English" title="English"/></a></li>
				</ul>
			</nav>
		<!-- Home -->
			<div class="wrapper wrapper-style1 wrapper-first">
				<article class="container" id="top">
					<div class="row">
						<div class="4u">
                                                    <span class="me">
                                                        <img width="217px" height="159px" src="../images/pcdreams.png" alt="" />
                                                    </span>
                                                    <!--<img style="position:absolute;left:20px;top:40px; z-index:200" src="images/stamp.png" alt="" />-->
						</div>
						<div class="8u">
							<header>
								<h1><strong>PC Dreams Software</strong></h1>
							</header>
							<p>Bem vindo à <strong>PC Dreams Software</strong>, sítio de bonitas aplicações.<br/>
                                                        O lugar onde os sonhos de software se tornam realidade!
                                                        </p>
							<a href="#work" class="button button-big">Saiba mais sobre o que fazemos</a>
						</div>
					</div>
				</article>
			</div>

		<!-- Work -->
			<div class="wrapper wrapper-style2">
				<article id="work">
					<header>
						<h2>Desenhamos e contruímos coisas fantásticas.</h2>
						<span>Sejam aplicações desktop, móveis, ou para a web. Você sonha, nós construímos!<br/>
                                                    Os nossos sonhos, as nossas aplicações. Os seus sonhos, as suas aplicações. 
                                                    <br/>Dê uma vista de olhos ao portfólio para ter uma ideia do que fazemos.</span>
                                                
					</header>
					<div class="container">
						<div class="row">
							<div class="4u">
								<section class="box box-style1">
									<!--<span class="icon featured-icon icon-comments-alt"></span>-->
                                                                        <img src="images/ic_launcher.png"/>
									<h3>EasyMessage</h3>
									<p class="align_justify">O EasyMessage é uma aplicação gratuita de envio de mensagens, disponivel para Android, iPhone e iPad.<br/>
                                                                        Permite-lhe enviar mensagens através de vários serviços (SMS & Email) e colocar nas redes sociais (Twitter & Facebook) ao mesmo tempo,
                                                                         escrevendo uma única vez. Pode gerir a sua audiência através da criação de grupos e guardar mensagens correntes no arquivo para reutilizar mais tarde.
                                                                        </p>
								</section>
							</div>
							<div class="4u">
								<section class="box box-style1">
									<!--<span class="icon featured-icon icon-file-alt"></span>-->
                                                                        <img src="images/logo_big.png"/>
									<h3>AEMS</h3>
									<p class="align_justify">AEMS (sigla para Advanced Event Management System) é o primeiro sistema 360º de gestão e registo em eventos. 
                                                                        Trata-te de um pacote completo e genérico que inclui funcionalidades de gestão de projecto e oferece aos organizadores de eventos a possibilidade de gerir todos os passos e recursos envolvidos no 
                                                                        planeamento, execução e análise do evento. 
                                                                        </p>
								</section>
							</div>
                                                        <div class="4u">
								<section class="box box-style1">
                                                                    <img src="images/flapi.png" width="100" height="100"/>
									<h3>Flapi - The Bird Fish!</h3>
									<p class="align_justify">Flapi - The Bird Fish é um divertido jogo para as plataformas móveis iPhone e iPad. 
                                                                           O Flapi é um peixinho dourado cheio de estilo cujo lago onde vivia acabou de secar, por isso ele não teve mais remédio senão fugir e procurar uma nova casa.
                                                                           Torna-te parte desta aventura com o Flapi, tenta levá-lo o mais longe possível evitando os obstáculos. Joga com os teus amigos e tenta bater recordes. Diversão garantida!
                                                                            <br/></p>
								</section>
							</div>
                                                        <div class="4u"><br/>
								<section class="box box-style1">
                                                                    <img src="images/clones.png" width="100" height="100"/>
									<h3>Revenge Of The Clones!</h3>
									<p class="align_justify">Revenge Of The Clones é um jogo para as plataformas móveis iPhone e iPad. 
                                                                            Este hilariante jogo é uma paródia ao sucesso do jogo Flappy bird e a todos os seus clones, incluindo <a target="_blank" href="https://itunes.apple.com/us/app/flapi-the-bird-fish/id837165900?ls=1&mt=8">Flapi - The Bird Fish</a>. 
                                                                           Desta vez os pequenos pássaros estão de volta para se vingarem. Chegam carregados de bombas e vão tentar destruir a cidade dos canos de esgoto. 
                                                                           Apenas a perícia do utilizador ao manobrar o cano voador pode salvar a cidade. Dê pancada nos pássaros e faça-os gritar ao serem atingidos pelas suas próprias bombas.
                                                                            <br/><br/></p>
								</section>
							</div>
                                                        <div class="4u"><br/>
								<section class="box box-style1">
                                                                    <img src="images/alien_player.png" width="100" height="100"/>
                                                                    <h3>Alfie - The Angry Alien Saga! </h3>
									<p class="align_justify">Alfie - The Angry Alien Saga é um divertido e difícil jogo do tipo "space shooter" para IOS (iPhone e iPad) e Android. 
                                                                            Alfie é um engraçado e destemido explorador alienígena que decidiu ir explorar um estranho planeta, recentemente descoberto. Infelizmente 
                                                                             o motor da sua nave foi danificado por um pequeno asteróide, próximo da órbita do planeta, e ele acabou for ficar encalhado num lugar cheio de perigos...
                                                                             <!--But the worst thing was that 
                                                                             the asteroids belt was habitated by strange (and very unfriendly) slug green creatures. These creatures are trying to kill Alfie's and hijack his spacecraft.
                                                                             The only way for Alfie to escape alive is to collect all the energy stars and repair the engine.-->
                                                                            <br/><br/></p>
								</section>
							</div>
                                                       <div class="4u"><br/>
								<section class="box box-style1">
                                                                    <img src="images/jelly.png" width="100" height="100"/>
                                                                    <h3>Jelly Crush Party! </h3>
									<p class="align_justify">Brevemente disponível para IOS (iPhone and iPad). 
                                                                            <img src="images/jellypromo.png" width="300" height="389"/>
                                                                            <br/><br/></p>
								</section>
							</div>
							
                                                    
						</div>
					</div>
					<footer>
                                            <h4>Lista de empresas com quem temos colaborado ao longo do tempo (*)</h4>
                                            
                                            <ul class="ul_left_align">
                                            <li>Glintt SA (Portugal)</li>
                                            <li>SAPO, PT Comunicações SA (Portugal)</li>
                                            <li>Opensource Strategies Inc. (United States)</li>
                                            <li>Simchronise Limited (Ireland)</li>
                                            <li>Tapastreet Limited (Ireland)</li>
                                            <li>Funambol Inc. (United States)</li>
                                            <li>Outros...</li>
                                            </ul>
                                            <span style="margin:0;padding:0">(*)Obs: Não é possível detalhar sobre estes projectos devido à existência de NDA's (Non Disclosure Agreements).</span>
                                            <br style="line-height:4px;"/>
					
						<a href="#portfolio" class="button button-big">Veja algum do nosso trabalho mais recente</a>
					</footer>
				</article>
			</div>

		<!-- Portfolio -->
			<div class="wrapper wrapper-style3">
                               <article id="portfolio">
					<header>
                                            <h2>
                                                <img style="vertical-align:middle; margin-left:5px" width="100" height="100" src="images/flapi.png"/>
                                                Flapi - The Bird Fish <sup>&trade;</sup><a href="https://itunes.apple.com/us/app/flapi-the-bird-fish/id837165900?ls=1&mt=8"><img style="vertical-align:middle; margin-left:5px" src="images/available_app_store_200px.png"/></a>
                                             </h2>
                                                
                                            <span>Flapi - The Fish Bird é um divertido jogo para plataformas móveis da Appe&trade; (iPhone e iPad)<br/>
                                                    Flapi é um peixe equipado com um "jet pack". Só tu com a tua perícia podes guiá-lo na sua derradeira tentativa de encontrar um novo charco para viver.
                                                </span>
                                                
					</header>
					<div class="container">
						<div class="row">
							<div class="12u">
							</div>
						</div>
						<div class="row">
							<div class="4u">
								<article class="box box-style2">
									<a href="https://itunes.apple.com/us/app/flapi-the-bird-fish/id837165900?ls=1&mt=8" class="image image-full"><img src="images/flapi5.png" alt="" /></a>
									<h3><a href="https://itunes.apple.com/us/app/flapi-the-bird-fish/id837165900?ls=1&mt=8">Touch & Go</a></h3>
									<p>Apenas com toques, de maneira simples e divertida, é possível guiar o Flapi em busca da sua nova casa.</p>
								</article>
							</div>
							<div class="4u">
								<article class="box box-style2">
									<a href="https://itunes.apple.com/us/app/flapi-the-bird-fish/id837165900?ls=1&mt=8" class="image image-full"><img src="images/flapi2.png" alt="" /></a>
									<h3><a href="https://itunes.apple.com/us/app/flapi-the-bird-fish/id837165900?ls=1&mt=8">Evita os Obstáculos</a></h3>
									<p>Deves evitar todos os obstáculos que aparecem no caminho e voar pelos intervalos.</p>
								</article>
							</div>
							<div class="4u">
								<article class="box box-style2">
									<a href="https://itunes.apple.com/us/app/flapi-the-bird-fish/id837165900?ls=1&mt=8" class="image image-full"><img src="images/flapi4.png" alt="" /></a>
									<h3><a href="https://itunes.apple.com/us/app/flapi-the-bird-fish/id837165900?ls=1&mt=8">Procura Desafios</a></h3>
									<p>Tenta ir o mais longe possível. Desafia os teus amigos a baterem o teu recorde.</p>
								</article>
							</div>
						</div>
						<!--<div class="row">
							
						</div>-->
					</div>
					<footer>
						<!--<p></p>
						<a href="#contact" class="button button-big">Get in touch with us</a>-->
					</footer>
				</article>
                            
                               <!-- revenge of clones -->
                               <article id="portfolio">
					<header>
                                            <h2>
                                                <img style="vertical-align:middle; margin-left:5px" width="100" height="100" src="images/clones.png"/>
                                                Revenge Of The Clones <sup>&trade;</sup><a href="https://itunes.apple.com/us/app/revenge-of-the-clones/id859897918?mt=8"><img style="vertical-align:middle; margin-left:5px" src="images/available_app_store_200px.png"/></a>
                                             </h2>
                                                
						<span>Revenge Of The Clones é um hilariante jogo para plataformas móveis da Appe&trade; (iPhone e iPad)<br/>
                                                    Trata-se de uma divertida paródia a um dos maiores sucessos de sempre (Flappy Bird e todos os seus clones).<br/> 
                                                    Desta feita os pássaros estão de volta e querem vingar-se dos canos de esgoto onde esbarravam.
                                                </span>
                                                
					</header>
					<div class="container">
						<div class="row">
							<div class="12u">
							</div>
						</div>
						<div class="row">
							<div class="4u">
								<article class="box box-style2">
									<a href="https://itunes.apple.com/us/app/revenge-of-the-clones/id859897918?mt=8" class="image image-full"><img src="images/clones1.png" alt="" /></a>
									<h3><a href="https://itunes.apple.com/us/app/revenge-of-the-clones/id859897918?mt=8">Mantém as bombas longe</a></h3>
									<p>Proteje a cidade dos canos de esgoto. Impede os pássaros bombistas de a destruirem.</p>
								</article>
							</div>
							<div class="4u">
								<article class="box box-style2">
									<a href="https://itunes.apple.com/us/app/revenge-of-the-clones/id859897918?mt=8" class="image image-full"><img src="images/clones2.png" alt="" /></a>
									<h3><a href="https://itunes.apple.com/us/app/revenge-of-the-clones/id859897918?mt=8">Acerta-lhes com pancada</a></h3>
									<p>Usa o cano voador como um taco de basebol. Dá-lhes pancada e afuguenta-os de vez.</p>
								</article>
							</div>
							<div class="4u">
								<article class="box box-style2">
									<a href="https://itunes.apple.com/us/app/revenge-of-the-clones/id859897918?mt=8" class="image image-full"><img src="images/clones3.png" alt="" /></a>
									<h3><a href="https://itunes.apple.com/us/app/revenge-of-the-clones/id859897918?mt=8">Fá-los fugir e gritar</a></h3>
									<p>Vira o feitiço contra o feiticeiro. Rebenta as bombas neles e fá-los fugir a gritar e a arder.</p>
								</article>
							</div>
						</div>
						<!--<div class="row">
							
						</div>-->
					</div>
					<footer>
						<!--<p></p>
						<a href="#contact" class="button button-big">Get in touch with us</a>-->
					</footer>
				</article>
                               
                               <!-- Alfie - The Angry Alien Saga -->
                               <article id="portfolio">
					<header>
                                            <h2>
                                                <img style="vertical-align:middle; margin-left:5px" width="100" height="100" src="images/alien_player.png"/>
                                                Alfie - The Angry Alien Saga <sup>&trade;</sup><a href="https://itunes.apple.com/us/app/alfie-the-angry-alien-saga/id827406444?mt=8"><img style="vertical-align:middle; margin-left:5px" src="images/available_app_store_200px.png"/></a>
                                                <a href="https://play.google.com/store/apps/details?id=com.pcristo.flappyalien"><img style="vertical-align:middle; margin-left:5px" src="images/en_generic_rgb_wo_60.png"/></a>
                                             </h2>
                                                
						<span>Alfie - The Angry Alien Saga é um divertido (e dificil) "space shooter" game for IOS e Android.<br/>
                                                    Na sua saga de exploração de novos mundos acabou por ficar preso num estranho lugar. <br/>Agora, tudo o que mais deseja é voltar a casa.
                                                </span>
                                                
					</header>
					<div class="container">
						<div class="row">
							<div class="12u">
							</div>
						</div>
						<div class="row">
							<div class="4u">
								<article class="box box-style2">
									<a href="https://itunes.apple.com/us/app/alfie-the-angry-alien-saga/id827406444?mt=8" class="image image-full"><img src="images/Alfie1.png" alt="" /></a>
									<h3><a href="https://itunes.apple.com/us/app/alfie-the-angry-alien-saga/id827406444?mt=8">Ajuda o Alfie a voltar a casa</a></h3>
									<p>Ajuda o Alfie a voltar são e salvo para junto da sua família. Que a força do jogo esteja contigo!</p>
								</article>
							</div>
							<div class="4u">
								<article class="box box-style2">
									<a href="https://itunes.apple.com/us/app/alfie-the-angry-alien-saga/id827406444?mt=8" class="image image-full"><img src="images/Alfie2.png" alt="" /></a>
									<h3><a href="https://itunes.apple.com/us/app/alfie-the-angry-alien-saga/id827406444?mt=8">Recolhe todas as estrelas de energia</a></h3>
									<p>Recolhe todas as estrelas de energia para poderes reparar a avaria do motor da nave e teres combustível.</p>
								</article>
							</div>
							<div class="4u">
								<article class="box box-style2">
									<a href="https://itunes.apple.com/us/app/alfie-the-angry-alien-saga/id827406444?mt=8" class="image image-full"><img src="images/Alfie3.png" alt="" /></a>
									<h3><a href="hhttps://itunes.apple.com/us/app/alfie-the-angry-alien-saga/id827406444?mt=8">Evita os inimigos</a></h3>
									<p>Extermina todas as lesmas verdes e demais alienígenas. Tem cuidado com o campo de asteróides e a restantes armadilhas.</p>
								</article>
							</div>
						</div>
						<!--<div class="row">
							
						</div>-->
					</div>
					<footer>
						<!--<p></p>
						<a href="#contact" class="button button-big">Get in touch with us</a>-->
					</footer>
				</article>
                            
				<article id="portfolio">
					<header>
                                            <h2>EasyMessage <sup>&trade;</sup><a href="https://itunes.apple.com/us/app/easymessage/id668776671?ls=1&mt=8"><img style="vertical-align:middle; margin-left:5px" src="images/available_app_store_200px.png"/></a>
                                                <a href="https://play.google.com/store/apps/details?id=com.pcristo"><img style="vertical-align:middle; margin-left:5px" src="images/en_generic_rgb_wo_60.png"/></a>
                                             </h2>
                                                
						<span>O EasyMessage é um utilitário que torna a troca de mensagens mais simples e divertida do que nunca!<br/>
                                                    Escreva apenas uma mensagem e a aplicação saberá exactamente como alcançar cada destinatário.<br/>
                                                    Email, SMS e redes socias juntos numa só aplicação!
                                                </span>
                                                
					</header>
					<div class="container">
						<div class="row">
							<div class="12u">
							</div>
						</div>
						<div class="row">
							<div class="4u">
								<article class="box box-style2">
									<a href="https://itunes.apple.com/us/app/easymessage/id668776671?ls=1&mt=8" class="image image-full"><img src="images/archive.png" alt="" /></a>
									<h3><a href="https://itunes.apple.com/us/app/easymessage/id668776671?ls=1&mt=8">Envie e arquive</a></h3>
									<p>Use mensagens pré-definidas para poupar tempo. Guarde as suas próprias mensagens.</p>
								</article>
							</div>
							<div class="4u">
								<article class="box box-style2">
									<a href="https://itunes.apple.com/us/app/easymessage/id668776671?ls=1&mt=8" class="image image-full"><img src="images/easy1.png" alt="" /></a>
									<h3><a href="https://itunes.apple.com/us/app/easymessage/id668776671?ls=1&mt=8">Escreva uma só vez</a></h3>
									<p>Envie para múltiplos contactos ao mesmo tempo, seja por Email, SMS, Facebook ou Twitter.</p>
								</article>
							</div>
							<div class="4u">
								<article class="box box-style2">
									<a href="https://itunes.apple.com/us/app/easymessage/id668776671?ls=1&mt=8" class="image image-full"><img src="images/easy2.png" alt="" /></a>
									<h3><a href="https://itunes.apple.com/us/app/easymessage/id668776671?ls=1&mt=8">Gira listas de contactos</a></h3>
									<p>Gira os seus contactos de uma forma fácil. Crie listas ou grupos.</p>
								</article>
							</div>
						</div>
						<!--<div class="row">
							
						</div>-->
					</div>
					<footer>
						<!--<p></p>
						<a href="#contact" class="button button-big">Get in touch with us</a>-->
					</footer>
				</article>
                                <!-- AEMS -->
                                <article id="portofolio">
                                <header>
                                            <h2><img style="vertical-align:middle; margin-left:5px" src="images/logo_big.png"/>AEMS - Advanced Event Management System</h2>
                                                
						<span>Sistema 360º de Gestão e Registo em Eventos com Funcionalidades de Gestão de Projectos!<br/>
                                                    <a href="http://www.advancedeventmanagement.com">www.advancedeventmanagement.com</a>
                                                </span>
                                                
					</header>
					<div class="container">
						<div class="row">
							<div class="12u">
							</div>
						</div>
						<div class="row">
							<div class="4u">
								<article class="box box-style2">
									<a href="http://www.advancedeventmanagement.com" class="image image-full"><img src="images/home_screen.png" alt="" /></a>
									<h3><a href="http://www.advancedeventmanagement.com">Configuração Zer0&trade; <br/></a></h3>
									<p>O AEMS funciona "Out-of-the-box". Não necessita de instalação ou configuração, é só descomprimir o zip e iníciar! O AEMS funciona com ou sem ligação à Internet.</p>
                                                                </article>
							</div>
							<div class="4u">
								<article class="box box-style2">
									<a href="http://www.advancedeventmanagement.com" class="image image-full"><img src="images/main_login_screen.png" alt="" /></a>
									<h3><a href="http://www.advancedeventmanagement.com">Genérico, Acessível e Configurável</a></h3>
									<p>O AEMS é "white label", o que significa que pode ser adaptado às suas preferências. Genérico e completo, pode ser usado para todos os tipos de eventos, por todo o tipo de empresa.
                                                                        
                                                                        </p>
								</article>
							</div>
							<div class="4u">
								<article class="box box-style2">
									<a href="http://www.advancedeventmanagement.com" class="image image-full"><img src="images/home_screen_imac.png" alt="" /></a>
									<h3><a href="http://www.advancedeventmanagement.com">Multi-plataforma</a></h3>
									<p>O AEMS funciona em Windows, Linux and Mac. Acessível em qualquer lado, em qualquer altura, seja no seu pc, portátil ou smartphone.</p>
								</article>
							</div>
						</div>
						
					</div>
					<footer>
						<p></p>
						<a href="#contact" class="button button-big">Entre em contacto connosco</a>
					</footer>
				</article>
			</div>

		<!-- Contact -->
			<div class="wrapper wrapper-style4">
				<article id="contact" class="container small">
					<header>
						<h2>Deseja contratar-nos? Contacte-nos!</h2>
                                                <span>Use o formulário abaixo ou envie-nos directamente um email: <a href="mailto:info@pcdreams-software.com">info[at]pcdreams-software.com </a>.</span>
					</header>
					<div>
						<div class="row">
							<div class="12u">
                                                            <form onsubmit="return validateForm();" method="post" name="formcontact" action="contact?action=contact">
									<div>
										<div class="row half">
											<div class="6u">
												<input type="text" name="name" id="name" placeholder="Name" />
											</div>
											<div class="6u">
												<input type="text" name="email" id="email" placeholder="Email" />
											</div>
										</div>
										<div class="row half">
											<div class="12u">
												<input type="text" name="subject" id="subject" placeholder="Subject" />
											</div>
										</div>
										<div class="row half">
											<div class="12u">
												<textarea name="message" id="message" placeholder="Message"></textarea>
											</div>
                                                                                    (*) Todos os campos são obrigatórios
										</div>
										<div class="row">
                                                                                    
											<div class="12u">
												<a href="#" class="button form-button-submit">Enviar Mensagem</a>
												<a href="#" class="button button-alt form-button-reset">Limpar Formulário</a>
											</div>
										</div>
									</div>
                                                                    
								</form>
                                                            
							</div>
						</div>
						<div class="row row-special">
							<div class="12u">
								<h3>Encontre-nos também em ...</h3>
								<ul class="social">
									<li class="twitter"><a href="https://twitter.com/pcdreamsapps" class="icon icon-twitter"><span>Twitter</span></a></li>
									<li class="facebook"><a href="https://www.facebook.com/pages/PC-Dreams-Software/557351897713700" class="icon icon-facebook"><span>Facebook</span></a></li>
									<!--<li class="dribbble"><a href="http://dribbble.com/n33" class="icon icon-dribbble"><span>Dribbble</span></a></li>-->
									<li class="linkedin"><a href="http://pt.linkedin.com/pub/paulo-cristo/4/b90/755/" class="icon icon-linkedin"><span>LinkedIn</span></a></li>
									<!--<li class="tumblr"><a href="#" class="icon icon-tumblr"><span>Tumblr</span></a></li>
									<li class="googleplus"><a href="#" class="icon icon-google-plus"><span>Google+</span></a></li>
									<li class="github"><a href="http://github.com/n33" class="icon icon-github"><span>Github</span></a></li>-->
									<!--
									<li class="rss"><a href="#" class="icon icon-rss"><span>RSS</span></a></li>
									<li class="instagram"><a href="#" class="icon icon-instagram"><span>Instagram</span></a></li>
									<li class="foursquare"><a href="#" class="icon icon-foursquare"><span>Foursquare</span></a></li>
									<li class="skype"><a href="#" class="icon icon-skype"><span>Skype</span></a></li>
									<li class="soundcloud"><a href="#" class="icon icon-soundcloud"><span>Soundcloud</span></a></li>
									<li class="youtube"><a href="#" class="icon icon-youtube"><span>YouTube</span></a></li>
									<li class="blogger"><a href="#" class="icon icon-blogger"><span>Blogger</span></a></li>
									<li class="flickr"><a href="#" class="icon icon-flickr"><span>Flickr</span></a></li>
									<li class="vimeo"><a href="#" class="icon icon-vimeo"><span>Vimeo</span></a></li>
									-->
								</ul>
							</div>
						</div>
					</div>
                                    <footer>
                                            <p id="copyright">
							&copy; 2014 PCDreams-Software  | Design baseado em : <a href="http://html5up.net/">HTML5 UP</a>
						</p>
				    </footer>
				</article>
			</div>


	</body>
</html>
