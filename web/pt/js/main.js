/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function validateForm() { 
    
    var name =document.forms["formcontact"]["name"].value;
    var subject =document.forms["formcontact"]["subject"].value;
    var message =document.forms["formcontact"]["message"].value;
    var email =document.forms["formcontact"]["email"].value;
    
    if (name==null || name=="" || subject==null || subject=="" 
            ||  message==null || message=="" || email==null || email=="")
    {
        alert("Please fill all fields first!");
        return false;
    }

    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if( re.test(email) == false ) {
        alert(email + " is not a valid email address!");
        return false;
    }
    return true;
}




