package com.pcdreams.pt;

/* Copyright 2010 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */




import com.sun.mail.smtp.SMTPTransport;

import java.util.Properties;
import javax.ejb.Asynchronous;
import javax.ejb.Singleton;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

@Singleton
public class MailEngine {

    //private static final String email = "advancedeventmanagement@gmail.com";
// private static final String oauthToken = "1/eXvqeHpc1-cjOCYFdYk3iLZ6u1bq1AOiu1yf81WYcU4";
// private static final String oauthTokenSecret = "-fzr-ekqd-51j7WdBFGGq96S";
    private static SMTPTransport transport = null;

    /**
     * @return the transport
     */
    public SMTPTransport getTransport() {
        return transport;
    }

    /**
     * @param aTransport the transport to set
     */
    public void setTransport(SMTPTransport aTransport) {
        transport = aTransport;
    }


    public MailEngine() {

    }


    private Properties getDefaultSMTPProperties(String host, int port, String userFrom) {
        Properties properties = new Properties();
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", port);
        properties.put("mail.smtp.from", userFrom);
        properties.put("mail.smtp.ehlo", "true");
        properties.put("mail.smtp.auth", "true");
        return properties;
    }

    private Properties getDefaultGmailProperties(String host, int port, String userFrom) {
        Properties props = new Properties();
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", port);
        props.put("mail.smtp.from", userFrom);
        props.put("mail.smtp.ehlo", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.starttls.required", "true");
        props.put("mail.smtp.sasl.enable", "false");
        return props;
    }

    /**
     * @param host
     * @param port
     * @param appSettings
     * @return
     * @throws Exception
     */
    private Session connectToGmailSmtp(String host, int port) throws Exception {

        Properties props = getDefaultGmailProperties(host, port, "advancedeventmanagement@gmail.com");
        Authenticator auth = new SMTPAuthenticator("advancedeventmanagement@gmail.com", "evsuhe2003aems");
        Session session = Session.getInstance(props, auth);
        session.setDebug(false);

        final URLName unusedUrlName = null;
        transport = new SMTPTransport(session, unusedUrlName);
        // If the password is non-null, SMTP tries to do AUTH LOGIN.
        final String emptyPassword = null;
        try {
            transport.connect(host, port, "advancedeventmanagement@gmail.com", emptyPassword);
        } catch (javax.mail.MessagingException ex) {
            //sometimes we might fail to resolve first name
            //System.out.print("ERROR " + ex.getMessage());
            if (ex.getNextException() instanceof java.net.UnknownHostException) {
                transport.connect("smtp.gmail.com", port, "advancedeventmanagement@gmail.com", emptyPassword);
            }
        } finally {
            return session;
        }
    }

    /**
     * @param host
     * @param port
     * @param appSettings
     * @return
     * @throws Exception
     */
    private Session connectToDefaultSmtp(String host, int port) throws Exception {

        Properties props = getDefaultSMTPProperties(host, port, "info@advancedeventmanagement.com");
        Authenticator auth = new SMTPAuthenticator("info@advancedeventmanagement.com", "evsuhe2003aems");
        Session session = Session.getInstance(props, auth);
        session.setDebug(false);

        final URLName unusedUrlName = null;
        transport = new SMTPTransport(session, unusedUrlName);
        // If the password is non-null, SMTP tries to do AUTH LOGIN.
        final String emptyPassword = null;
        try {
            transport.connect(host, port, "info@advancedeventmanagement.com", emptyPassword);
        } catch (javax.mail.MessagingException ex) {
            //TODO JUST LOG IT HERE
            //System.out.print("javax.mail.MessagingException: " + ex.getMessage());
        } finally {
            return session;
        }
    }


    /**
     * SimpleAuthenticator is used to do simple authentication
     * when the SMTP server requires it.
     */
    private static class SMTPAuthenticator extends javax.mail.Authenticator {
        //private final String SMTP_AUTH_USER ="advancedeventmanagement@gmail.com";
        //private final String SMTP_AUTH_PWD = "evsuhe2003";

        private String username;
        private String password;

        @Override
        public PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication(username, password);
        }

        public PasswordAuthentication getPasswordAuthentication(String user, String pass) {

            return new PasswordAuthentication(user, pass);
        }

        public SMTPAuthenticator(String user, String pass) {
            this.username = user;
            this.password = pass;
        }


    }

    //"advancedeventmanagement@gmail.com",
    @Asynchronous
    public void sendEmailing(
            String mailDestination,
            String subject,
            //String fromAddress,
            String body) throws Exception {


        Session session = connectToGmailSmtp("smtp.googlemail.com", 587);

        if (session == null)  //try 2nd approach
            session = connectToDefaultSmtp("mail.noip.com", 465);


        if (session != null) {
            Message message = new MimeMessage(session);
            message.setSubject(subject);
            message.setContent(body,"text/html");
            InternetAddress arpaAddress = new InternetAddress(mailDestination);
            message.setFrom(new InternetAddress("info@advancedeventmanagement.com"));
            message.setRecipient(Message.RecipientType.TO, arpaAddress);
            SMTPTransport.send(message);

        }


    }

}
  
 
